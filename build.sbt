name := "starter"
version := "1.0"

scalaVersion := "2.13.1"
scalacOptions := Seq("-feature", "-deprecation")
ThisBuild / turbo := true

routesGenerator := InjectedRoutesGenerator

val circeVersion = "0.12.1"

libraryDependencies ++= Seq(
  jdbc, ehcache, guice, ws
  , "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
  , "com.typesafe.play" %% "play-slick" % "4.0.2"
  , "org.typelevel" %% "cats-core" % "2.0.0"
  , "net.codingwell" %% "scala-guice" % "4.2.6"
  , "org.postgresql" % "postgresql" % "42.1.3"
  , "io.circe" %% "circe-core" % circeVersion
  , "io.circe" %% "circe-generic" % circeVersion
  , "io.circe" %% "circe-parser" % circeVersion
  , "io.circe" %% "circe-generic-extras" % circeVersion
  , "dev.zio" %% "zio" % "1.0.0-RC15"
  , "dev.zio" %% "zio-streams" % "1.0.0-RC15"
)

libraryDependencies ++= Seq(
  "com.h2database" % "h2" % "1.4.197" % Test
  , "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test
)

excludeDependencies ++= Seq(
  ExclusionRule("com.typesafe.play", "play-java")
  , ExclusionRule("com.typesafe.play", "play-json")
)

lazy val root = (project in file(".")).enablePlugins(PlayScala)
