import mocks.UserMock
import persists.UserPersist
import persists.postgres.UserTable

final class UserPersistSpec
  extends UnitSpec {

  private val userPersist = as[UserPersist]

  "list" should {
    "be right" in {
      val result = waitResult(userPersist.list)

      result.isRight mustBe true
      result.map(list => list contains UserMock.user1 mustBe true)
    }
  }

  "get" should {
    "be right when given valid id" in {
      val result = waitResult(userPersist.get(UserMock.user1.id))

      result.isRight mustBe true
      result.map(user => user.id mustBe UserMock.user1.id)
    }

    "not right when given invalid id" in {
      val result = waitResult(userPersist.get("INVALID_ID"))

      result.isRight mustBe false
    }
  }

  "insert" should {
    "be right when given valid entity" in {
      val result = waitResult(userPersist.insert(UserMock.user1.copy("NEW_ID")))

      result.isRight mustBe true
    }

    "not right when given invalid entity" in {
      val result = waitResult(userPersist.insert(UserMock.user1))

      result.isRight mustBe false
    }
  }

  "update" should {
    "be right when given valid entity" in {
      val result = waitResult(userPersist.update(UserMock.user1.copy(password = "eiei")))

      result.isRight mustBe true
    }

    "not right when given invalid entity" in {
      val result = waitResult(userPersist.update(UserMock.user1.copy(id = "INVALID_ID", password = "eiei")))

      result.isRight mustBe false
    }
  }

  "delete" should {
    "be right when given valid id" in {
      val result = waitResult(userPersist.delete(UserMock.user1.id))

      result.isRight mustBe true
    }

    "not right when given invalid id" in {
      val result = waitResult(userPersist.delete("INVALID_ID"))

      result.isRight mustBe false
    }
  }

  private val userTable = TableQuery[UserTable]

  override protected def initSQL(): DBIO[_] = DBIO.seq(
    userTable.schema.dropIfExists
    , userTable.schema.create
    , userTable.forceInsertAll(UserMock.entities)
  )

}
