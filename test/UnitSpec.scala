import com.typesafe.scalalogging.LazyLogging
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.db.slick.DatabaseConfigProvider
import play.api.inject.Module
import play.api.inject.guice.GuiceApplicationBuilder
import slick.jdbc.H2Profile

import scala.concurrent.duration._
import scala.concurrent.{Await, Awaitable, ExecutionContext}
import scala.reflect.ClassTag

trait UnitSpec
  extends PlaySpec
    with GuiceOneAppPerSuite
    with BeforeAndAfterEach
    with H2Profile.API
    with LazyLogging {

  protected implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  protected def as[T: ClassTag]: T = app.injector.instanceOf[T]

  implicit override lazy val app: Application = new GuiceApplicationBuilder()
    .configure("slick.dbs.default.profile" -> "slick.jdbc.H2Profile$")
    .configure("slick.dbs.default.db.url" -> "jdbc:h2:mem:play;MODE=PostgreSQL")
    .overrides(overrideMockModules)
    .build

  private lazy val overrideMockModules = Seq.empty[Module]

  protected val dbConfigProvider: DatabaseConfigProvider = as[DatabaseConfigProvider]
  protected val dbConfig = dbConfigProvider.get[H2Profile]
  protected val db = dbConfig.db

  override def beforeEach: Unit = {
    Await.ready(db.run(initSQL()), Duration.Inf)

    preExecute()
  }

  override def afterEach: Unit = {
    Await.ready(db.run(postSQL()), Duration.Inf)
  }

  protected def initSQL(): DBIO[_] = DBIO.seq()

  protected def postSQL(): DBIO[_] = DBIO.seq()

  protected def preExecute(): Unit = {

  }

  protected implicit val duration: FiniteDuration = 10.seconds

  protected def waitResult[T](awaitable: Awaitable[T])
                             (implicit duration: Duration): T = {
    Await.result(awaitable, duration)
  }

}
