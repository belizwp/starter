import mocks.{ProductMock, UserMock}
import persists.postgres.{ProductTable, UserTable}
import play.api.test.FakeRequest
import play.api.test.Helpers._

final class ZProductControllerSpec
  extends UnitSpec {

  "GET /zproducts" should {
    "return 200 with user list" in {
      val result = route(app
        , FakeRequest(GET, "/zproducts")
      ).get

      status(result) mustBe OK
      contentAsString(result) must include(UserMock.user1.username)
      contentAsString(result) must include(UserMock.user2.username)
    }
  }

  private val userTable = TableQuery[UserTable]
  private val productsTable = TableQuery[ProductTable]

  override protected def initSQL(): DBIO[_] = DBIO.seq(
    userTable.schema.dropIfExists
    , userTable.schema.create
    , userTable.forceInsertAll(UserMock.entities)
    , productsTable.schema.dropIfExists
    , productsTable.schema.create
    , productsTable.forceInsertAll(ProductMock.entities)
  )

}
