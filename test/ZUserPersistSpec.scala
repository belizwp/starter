import definitions.exceptions.CannotReadException
import mocks.UserMock
import persists.ZUserPersist
import persists.postgres.UserTable

final class ZUserPersistSpec
  extends UnitSpec {

  private val userPersist = as[ZUserPersist]

  "list" should {
    "be right" in {
      userPersist.list map { list =>
        list contains UserMock.user1 mustBe true
      }
    }
  }

  "get" should {
    "be right when given valid id" in {
      userPersist.get(UserMock.user1.id) map { user =>
        user.id mustBe UserMock.user1.id
      }
    }

    "not right when given invalid id" in {
      userPersist.get("INVALID_ID") mapError { t =>
        t.isInstanceOf[CannotReadException] mustBe true
      }
    }
  }

  private val userTable = TableQuery[UserTable]

  override protected def initSQL(): DBIO[_] = DBIO.seq(
    userTable.schema.dropIfExists
    , userTable.schema.create
    , userTable.forceInsertAll(UserMock.entities)
  )

}
