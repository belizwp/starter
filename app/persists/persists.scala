import java.sql.Timestamp

import io.circe.parser._
import io.circe.syntax._
import org.joda.time.DateTime
import slick.jdbc.PostgresProfile.api._

package object persists {

  implicit def dateTime = MappedColumnType.base[DateTime, Timestamp](
    dateTime => new Timestamp(dateTime.getMillis),
    timeStamp => new DateTime(timeStamp.getTime)
  )

  implicit def stringList = MappedColumnType.base[List[String], String](
    list => list.asJson.noSpaces,
    json => decode[List[String]](json).getOrElse(List.empty)
  )

}
