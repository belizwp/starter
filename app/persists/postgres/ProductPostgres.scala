package persists.postgres

import definitions.exceptions._
import entities.ProductEntity
import javax.inject.Inject
import persists.{BasePersist, ProductPersist}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class ProductTable(tag: Tag) extends Table[ProductEntity](tag, "products") {

  def id = column[String]("id", O.PrimaryKey)

  def name = column[String]("name")

  def price = column[Long]("price")

  def userId = column[String]("user_id")

  override def * =
    (id, name, price, userId) <> (ProductEntity.tupled, ProductEntity.unapply)
}

class ProductPostgres @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
                               (implicit executionContext: ExecutionContext)
  extends BasePersist
    with ProductPersist {

  val users = TableQuery[ProductTable]

  override def list: Future[Either[PersistException, Seq[ProductEntity]]] = db.run {
    users.result.asTry
  } flatMap {
    case Success(list) => rightFE(list)
    case Failure(t)    =>
      logger.error("Error occur when read", t)
      leftFE(CannotReadException("Cannot list product"))
  }

  override def get(id: String): Future[Either[PersistException, ProductEntity]] = db.run {
    users.filter(_.id === id)
      .result
      .head
      .asTry
  } flatMap {
    case Success(entity) => rightFE(entity)
    case Failure(t)      =>
      logger.error("Error occur when read", t)
      leftFE(CannotReadException(s"Cannot get product id: $id"))
  }

}
