package persists.postgres

import definitions.exceptions.{CannotReadException, CannotUpdateException}
import entities.UserEntity
import javax.inject.Inject
import persists.{BasePersist, ZUserPersist}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.PostgresProfile.api._
import zio.Task

import scala.concurrent.Future

class ZUserPostgres @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends BasePersist
    with ZUserPersist {

  val users = TableQuery[UserTable]

  override def list: Task[Seq[UserEntity]] = zio { implicit ec =>
    val query = users.result

    db.run(query)
  }

  override def get(id: String): Task[UserEntity] = zio { implicit ec =>
    val query = users.filter(_.id === id)
      .result
      .headOption

    db.run(query) flatMap {
      case Some(e) => Future.successful(e)
      case None    => Future.failed(CannotReadException(id))
    }
  }

  override def update(e: UserEntity): Task[UserEntity] = zio { implicit ec =>
    val query = users.filter(_.id === e.id)
      .update(e)

    db.run(query) flatMap {
      case 1 => Future.successful(e)
      case _ => Future.failed(CannotUpdateException(e.id))
    }
  }

}
