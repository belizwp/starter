package persists.postgres

import definitions.exceptions._
import entities.UserEntity
import javax.inject.Inject
import persists.{BasePersist, UserPersist}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class UserTable(tag: Tag) extends Table[UserEntity](tag, "users") {

  def id = column[String]("id", O.PrimaryKey)

  def username = column[String]("username")

  def password = column[String]("password")

  override def * =
    (id, username, password) <> (UserEntity.tupled, UserEntity.unapply)
}

class UserPostgres @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
                            (implicit executionContext: ExecutionContext)
  extends BasePersist
    with UserPersist {

  val users = TableQuery[UserTable]

  override def list: Future[Either[PersistException, Seq[UserEntity]]] = db.run {
    users.result.asTry
  } flatMap {
    case Success(list) => rightFE(list)
    case Failure(t)    =>
      logger.error("Error occur when read", t)
      leftFE(CannotReadException("Cannot list users"))
  }

  override def get(id: String): Future[Either[PersistException, UserEntity]] = db.run {
    users.filter(_.id === id)
      .result
      .head
      .asTry
  } flatMap {
    case Success(entity) => rightFE(entity)
    case Failure(t)      =>
      logger.error("Error occur when read", t)
      leftFE(CannotReadException(s"Cannot get user id: $id"))
  }

  override def insert(entity: UserEntity): Future[Either[PersistException, UserEntity]] = db.run {
    (users += entity).asTry
  } flatMap {
    case Success(_) => rightFE(entity)
    case Failure(t) =>
      logger.error("Error occur when insert", t)
      leftFE(CannotCreateException(s"Cannot insert user id: ${entity.id}"))
  }

  override def update(entity: UserEntity): Future[Either[PersistException, UserEntity]] = db.run {
    users.filter(_.id === entity.id)
      .update(entity).asTry
  } flatMap {
    case Success(row) => if (row == 1) rightFE(entity) else leftFE(CannotUpdateException(entity.toString))
    case Failure(t)   =>
      logger.error("Error occur when update", t)
      leftFE(CannotUpdateException(s"Cannot update user id: ${entity.id}"))
  }

  override def delete(id: String): Future[Either[PersistException, Unit]] = db.run {
    users.filter(_.id === id)
      .delete
      .asTry
  } flatMap {
    case Success(row) => if (row == 1) rightFE(()) else leftFE(CannotDeleteException(id))
    case Failure(t)   =>
      logger.error("Error occur when delete", t)
      leftFE(CannotDeleteException(s"Cannot update user id: $id"))
  }

}
