package persists.postgres

import entities.ProductEntity
import javax.inject.Inject
import persists.{BasePersist, ZProductPersist}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.PostgresProfile.api._
import zio.Task

class ZProductPostgres @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends BasePersist
    with ZProductPersist {

  val products = TableQuery[ProductTable]

  override def list: Task[Seq[ProductEntity]] = zio { implicit ec =>
    db.run(products.result)
  }

  override def get(id: String): Task[ProductEntity] = zio { implicit ec =>
    db.run(products.filter(_.id === id).result.head)
  }

}
