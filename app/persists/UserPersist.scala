package persists

import definitions.exceptions.PersistException
import entities.UserEntity

import scala.concurrent.Future

trait UserPersist {

  def list: Future[Either[PersistException, Seq[UserEntity]]]

  def get(id: String): Future[Either[PersistException, UserEntity]]

  def insert(entity: UserEntity): Future[Either[PersistException, UserEntity]]

  def update(entity: UserEntity): Future[Either[PersistException, UserEntity]]

  def delete(id: String): Future[Either[PersistException, Unit]]

}
