package persists

import definitions.exceptions.PersistException
import entities.ProductEntity

import scala.concurrent.Future

trait ProductPersist {

  def list: Future[Either[PersistException, Seq[ProductEntity]]]

  def get(id: String): Future[Either[PersistException, ProductEntity]]

}
