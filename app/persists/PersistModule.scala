package persists

import persists.postgres.{ProductPostgres, UserPostgres, ZProductPostgres, ZUserPostgres}
import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

final class PersistModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[UserPersist].to[UserPostgres]
    , bind[ProductPersist].to[ProductPostgres]
    , bind[ZUserPersist].to[ZUserPostgres]
    , bind[ZProductPersist].to[ZProductPostgres]
  )
}
