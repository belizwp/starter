package persists

import com.typesafe.scalalogging.LazyLogging
import definitions.exceptions.PersistException
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.PostgresProfile
import zio.{Task, ZIO}

import scala.concurrent.{ExecutionContext, Future}

trait BasePersist
  extends HasDatabaseConfigProvider[PostgresProfile]
    with LazyLogging {

  def rightFE[T](result: T): Future[Either[PersistException, T]] = {
    Future.successful(Right(result))
  }

  def leftFE(result: PersistException): Future[Either[PersistException, Nothing]] = {
    Future.successful(Left(result))
  }

  protected def zio[A](make: ExecutionContext => scala.concurrent.Future[A]): Task[A] = ZIO.fromFuture(make)

}
