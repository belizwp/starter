package persists

import entities.ProductEntity
import zio.Task

trait ZProductPersist {

  def list: Task[Seq[ProductEntity]]

  def get(id: String): Task[ProductEntity]

}
