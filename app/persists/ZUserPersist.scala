package persists

import entities.UserEntity
import zio.Task

trait ZUserPersist {

  def list: Task[Seq[UserEntity]]

  def get(id: String): Task[UserEntity]

  def update(e: UserEntity): Task[UserEntity]

}
