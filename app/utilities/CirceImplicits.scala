package utilities

import io.circe.generic.extras.{AutoDerivation, Configuration}
import io.circe.Decoder.Result
import io.circe._
import org.joda.time.DateTime

trait CirceImplicits
  extends AutoDerivation
    with ConfigurationImplicits
    with DateTimeImplicits

object CirceImplicits extends CirceImplicits

private[utilities] trait ConfigurationImplicits {

  implicit val configuration: Configuration = Configuration.default.withSnakeCaseMemberNames

}

private[utilities] trait DateTimeImplicits {

  implicit val dateTimeEncoder: Encoder[DateTime] = new Encoder[DateTime] {
    override def apply(dateTime: DateTime): io.circe.Json = io.circe.Json.fromString(
      DateTimeFormatter.printDateTime(dateTime)
    )
  }

  implicit val dateTimeDecoder: Decoder[DateTime] = new Decoder[DateTime] {
    override def apply(cursor: HCursor): Result[DateTime] = cursor.value.asString match {
      case Some(dateTime) => Right(DateTimeFormatter.toDateWithTime(dateTime))
      case None => Left(DecodingFailure("org.joda.time.DateTime", cursor.history))
    }
  }

}
