package utilities

import com.typesafe.config.ConfigFactory

import scala.util.Try

object Config {

  private val config = ConfigFactory.load()

  def apply(key: String): String = config.getString(key)

  def apply(key: String, default: String): String = Try(config.getString(key)).getOrElse(default)

  def bool(key: String): Boolean = config.getBoolean(key)

  def bool(key: String, default: Boolean): Boolean = Try(config.getBoolean(key)).getOrElse(default)

}
