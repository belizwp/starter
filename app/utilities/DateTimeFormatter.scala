package utilities

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

trait DateTimeFormatter {

  def printDate(date: DateTime): String = DTF_DATE.print(date)

  def printDateTime(date: DateTime): String = DTF_DATETIME.print(date)

  def printDateTimeForFileName(date: DateTime): String = UPDATE_FILE_DATE.print(date)

  def printDateForWeb(date: DateTime): String = WEB_DATE.print(date)

  def printDateTimeForWeb(date: DateTime): String = WEB_DATE_TIME.print(date)

  def printKkDate(date: DateTime): String = KK_DATE.print(date)

  def printKkDateTime(date: DateTime): String = KK_DATE_TIME.print(date)

  def toDate(date: String): DateTime = DTF_DATE.parseDateTime(date)

  def toDateTimeWithTimeZone(date: String): DateTime = DTF_DATETIME_TIMEZONE.parseDateTime(date)

  def toDateWithTime(date: String): DateTime = DTF_DATETIME.parseDateTime(date)

  def toDateFromWeb(date: String): DateTime = WEB_DATE.parseDateTime(date)

  def toKkDate(date: String): DateTime = KK_DATE.parseDateTime(date)

  private val DTF_DATE = DateTimeFormat.forPattern("yyyy-MM-dd")

  private val DTF_DATETIME = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  private val DTF_DATETIME_TIMEZONE = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm Z")

  private val WEB_DATE = DateTimeFormat.forPattern("dd-MM-yyyy")

  private val WEB_DATE_TIME = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm")

  private val UPDATE_FILE_DATE = DateTimeFormat.forPattern("yyyyMMdd_HHmmss")

  private val KK_DATE = DateTimeFormat.forPattern("yyyyMMdd")

  private val KK_DATE_TIME = DateTimeFormat.forPattern("yyyyMMddHHmmssSSS")

}

object DateTimeFormatter extends DateTimeFormatter
