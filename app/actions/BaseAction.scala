package actions

import java.util.concurrent.TimeUnit

import com.google.common.base.Stopwatch
import com.typesafe.scalalogging.LazyLogging
import definitions.system.HttpHeaders._
import play.api.http.HttpVerbs
import play.api.mvc._

abstract class BaseAction[R[_] <: WrappedRequest[_]](parsers: PlayBodyParsers)
  extends ActionBuilder[R, AnyContent]
    with HttpVerbs
    with LazyLogging {

  val parser: BodyParser[AnyContent] = parsers.anyContent

  protected def processTime[R, A](request: Request[A])(block: => R): R = {
    val stopwatch = Stopwatch.createStarted()
    val userAgent = request.headers.get(AGENT).getOrElse("N/A")
    try {
      block
    } finally {
      logger.info(s"${request.method} ${request.uri} in ${stopwatch.stop.elapsed(TimeUnit.MILLISECONDS)} ms. [$userAgent]")
    }
  }

  protected def cache(result: Result): Result =
    result.withHeaders(CACHE_CONTROL -> MAX_AGE)

  protected def cookie(result: Result, cookie: Cookie): Result =
    result.withCookies(cookie)
      .bakeCookies()

}
