package actions

import javax.inject.{Inject, Singleton}
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

final case class ProfilingRequest[A](request: Request[A])
  extends WrappedRequest(request)

@Singleton
final class ProfilingAction @Inject()(playBodyParser: PlayBodyParsers)
                                     (implicit val executionContext: ExecutionContext)
  extends BaseAction[ProfilingRequest](playBodyParser) {

  def invokeBlock[A](request: Request[A], block: ProfilingRequest[A] => Future[Result]): Future[Result] = processTime(request) {
    block(ProfilingRequest(request)).map(cache)
  }

}
