package actions

import play.api.mvc.{Action, ActionBuilder, Result}
import zio.{DefaultRuntime, IO, UIO}

trait ZIOAction
  extends DefaultRuntime {

  implicit class ActionBuilderOps[+R[_], B](actionBuilder: ActionBuilder[R, B]) {

    def zio[E](zioActionBody: R[B] => UIO[Result]): Action[B] = actionBuilder.async { request =>
      unsafeRunToFuture(
        zioActionBody(request)
      )
    }
  }

  implicit class RecoverIO[E, A](io: IO[E, A]) {
    def recover(f: E => A): UIO[A] = io.fold(f, identity)
  }

}
