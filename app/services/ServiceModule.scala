package services

import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

final class ServiceModule extends Module {

  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq()

}
