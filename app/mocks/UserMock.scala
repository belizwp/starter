package mocks

import entities.UserEntity

object UserMock {

  val user1 = UserEntity("ID1", "admin", "password")

  val user2 = UserEntity("ID2", "user", "password")

  val user3 = UserEntity("ID3", "hacker", "password")

  val entities: List[UserEntity] = List(user1, user2, user3)

}
