package mocks

import entities.ProductEntity

object ProductMock {

  val p1 = ProductEntity("ID1", "p1", 100, UserMock.user1.id)

  val p2 = ProductEntity("ID2", "p2", 100, UserMock.user2.id)

  val entities: List[ProductEntity] = List(p1, p2)

}
