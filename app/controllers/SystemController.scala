package controllers

import actions.ProfilingAction
import definitions.system.SystemMessage._
import javax.inject.Inject
import messages.system.SystemStatusMessage
import play.api.mvc.{Action, AnyContent}

final class SystemController @Inject()(profilingAction: ProfilingAction)
  extends ApiController {

  def systemVersion: Action[AnyContent] = profilingAction {
    handling(SystemStatusMessage(
      SERVER_VERSION, SERVER_NAME
    ))
  }

}
