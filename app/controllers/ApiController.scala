package controllers

import actions.ZIOAction
import com.typesafe.scalalogging.LazyLogging
import definitions.exceptions._
import definitions.system.HttpHeaders._
import io.circe.Encoder
import io.circe.syntax._
import messages.Message
import messages.system.ErrorMessage
import play.api.i18n.I18nSupport
import play.api.mvc._
import utilities.CirceImplicits
import zio.{IO, UIO}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

trait ApiController
  extends InjectedController
    with LazyLogging
    with I18nSupport
    with CirceImplicits
    with ZIOAction {

  protected implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  protected def handling[T <: Message](action: => T)(implicit encoder: Encoder[T]): Result = {
    Try(ok(action)) match {
      case Failure(t)      => handlingThrowableResult(t).asApplicationJson
      case Success(result) => result.asApplicationJson
    }
  }

  protected def handling[T <: Message](action: => Future[Either[Throwable, T]])
                                      (implicit encoder: Encoder[T]): Future[Result] = {
    action flatMap {
      case Left(t)        => Future.successful(handlingThrowableResult(t).asApplicationJson)
      case Right(message) => Future(ok(message).asApplicationJson)
    }
  }

  protected def handling[E, T <: Message](action: => IO[Throwable, T])
                                         (implicit encoder: Encoder[T]): UIO[Result] = {
    action.fold(
      throwable => handlingThrowableResult(throwable).asApplicationJson
      , message => ok(message).asApplicationJson
    )
  }

  protected def httpStatus[T](action: => T)(result: Result): Result = {
    Try(action) match {
      case Failure(t) => handlingThrowableResult(t).asApplicationJson
      case Success(_) => result
    }
  }

  protected def httpStatus[T](action: => Future[Either[Throwable, T]])
                             (result: Result): Future[Result] = {
    action flatMap {
      case Left(t)  => Future.successful(handlingThrowableResult(t).asApplicationJson)
      case Right(_) => Future.successful(result)
    }
  }

  protected def httpStatus[T](action: => IO[Throwable, T])
                             (result: Result): UIO[Result] = {
    action.fold(
      throwable => handlingThrowableResult(throwable).asApplicationJson
      , _       => result
    )
  }

  private def handlingThrowableResult[T](failureResult: Throwable): Result = {
    failureResult match {
      case _: NotAuthenticatedException => Unauthorized
      case e: BadRequestException       => error(BadRequest, 400, e.getMessage)
      case e: NotFoundException         => error(NotFound, 404, e.getMessage)
      case e: ForbiddenRequestException => error(Forbidden, 403, e.getMessage)
      case e: PersistException          => error(InternalServerError, 500, e.getMessage)
      case NonFatal(t)                  => logger.error("Unexpected error occurred", t)
        error(InternalServerError, 500, "Something went wrong on our server, please contact us.")
    }
  }

  protected def ok[T <: Message](m: T)(implicit encoder: Encoder[T]): Result = Ok(m.asJson.noSpaces).asApplicationJson

  protected def error(status: Status, code: Int, message: String): Result = status(ErrorMessage(code, message).asJson.noSpaces).asApplicationJson

  implicit class ResultImprovement(result: Result) {

    def asApplicationJson: Result = result.as(APPLICATION_JSON)

  }

}
