package controllers

import actions.ProfilingAction
import facades.ZProductFacade
import javax.inject.Inject
import messages.{MyProductListMessage, MyProductMessage}
import play.api.mvc.{Action, AnyContent}

final class ZProductController @Inject()(profilingAction: ProfilingAction
                                         , facade: ZProductFacade)
  extends ApiController {

  def list: Action[AnyContent] = profilingAction.zio { implicit request =>
    handling(facade.list.map(list =>
      MyProductListMessage(list.map(MyProductMessage.apply)))
    )
  }

}
