package controllers

import actions.ProfilingAction
import cats.data.EitherT
import cats.implicits._
import facades.ProductFacade
import javax.inject.Inject
import messages.{MyProductListMessage, MyProductMessage}
import play.api.mvc.{Action, AnyContent}

final class ProductController @Inject()(profilingAction: ProfilingAction
                                        , facade: ProductFacade)
  extends ApiController {

  def list: Action[AnyContent] = profilingAction.async {
    handling(EitherT(facade.list).map(list =>
      MyProductListMessage(list.map(MyProductMessage.apply))).value
    )
  }

}
