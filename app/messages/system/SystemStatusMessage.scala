package messages.system

import messages.Message

final case class SystemStatusMessage(version: String
                                     , message: String)
  extends Message
