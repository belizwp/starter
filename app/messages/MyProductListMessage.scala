package messages

import models.MyProduct

final case class MyProductListMessage(products: List[MyProductMessage])
  extends Message

final case class MyProductMessage(id: String
                                  , name: String
                                  , price: Long
                                  , seller: String)

object MyProductMessage {

  def apply(model: MyProduct): MyProductMessage =
    MyProductMessage(
      id = model.id
      , name = model.name
      , price = model.price
      , seller = model.seller
    )

}
