package facades

import cats.data.EitherT
import cats.implicits._
import definitions.exceptions.{FacadeException, PersistException}
import entities.{ProductEntity, UserEntity}
import javax.inject.Inject
import models.MyProduct
import persists.{ProductPersist, UserPersist}

import scala.concurrent.Future

final class ProductFacade @Inject()(userPersist: UserPersist
                                    , productPersist: ProductPersist)
  extends BaseFacade {

  def list: Future[Either[FacadeException, List[MyProduct]]] = {
    (for {
      products   <- EitherT(productPersist.list)
      users      <- EitherT(getUsersForProducts(products.toList))
      myProducts <- EitherT(myProducts(products, users))
    } yield myProducts).value
  }

  private def getUsersForProducts(products: List[ProductEntity]): Future[Either[PersistException, List[UserEntity]]] = {
    @scala.annotation.tailrec
    def getUsers(ps: List[ProductEntity]
                 , us: List[Future[Either[PersistException, UserEntity]]]):
    List[Future[Either[PersistException, UserEntity]]] = ps match {
      case Nil => us
      case p :: ps => getUsers(ps, us.appended(userPersist.get(p.userId)))
    }

    Future.sequence(getUsers(products, List.empty))
      .map(sequenceE)
  }

  private def myProducts(ps: Seq[ProductEntity]
                 , us: Seq[UserEntity]): Future[Either[FacadeException, List[MyProduct]]] = Future {
    (ps zip us).map(tup => MyProduct(tup._1, tup._2))
      .toList
      .asRight[FacadeException]
  }

}
