package facades

import definitions.exceptions.PersistException
import entities.{ProductEntity, UserEntity}
import javax.inject.Inject
import models.MyProduct
import persists.{ZProductPersist, ZUserPersist}
import zio.{Task, ZIO}

final class ZProductFacade @Inject()(userPersist: ZUserPersist
                                     , productPersist: ZProductPersist)
  extends BaseFacade {

  def list: Task[List[MyProduct]] = {
    (for {
      products   <- productPersist.list
      users      <- getUsersForProducts(products.toList)
      myProducts <- myProducts(products, users)
    } yield {
      myProducts
    }) mapError {
      case _: PersistException => new Exception("Some persist error")
      case _ => new Exception("Custom unexpected error")
    }
  }

  private def getUsersForProducts(products: List[ProductEntity]): Task[List[UserEntity]] = {
    @scala.annotation.tailrec
    def getUsers(ps: List[ProductEntity], us: List[Task[UserEntity]]): List[Task[UserEntity]] = ps match {
      case Nil => us
      case p :: ps => getUsers(ps, us.appended(userPersist.get(p.userId)))
    }

    ZIO.sequence(getUsers(products, List.empty))
  }

  private def myProducts(ps: Seq[ProductEntity], us: Seq[UserEntity]): Task[List[MyProduct]] = ZIO.effect {
    (ps zip us).map(tup => MyProduct(tup._1, tup._2)).toList
  }

}
