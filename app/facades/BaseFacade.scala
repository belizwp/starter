package facades

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.ExecutionContext

trait BaseFacade
  extends LazyLogging {

  protected implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  protected def sequenceE[L, R](f: List[Either[L, R]]): Either[L, List[R]] = {
    f.foldRight(Right(Nil): Either[L, List[R]]) { (elem, acc) =>
      acc.flatMap(list => elem.map(_ :: list))
    }
  }

}
