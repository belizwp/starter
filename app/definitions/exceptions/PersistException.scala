package definitions.exceptions

sealed abstract class PersistException(message: String)
  extends InternalErrorException(message)

final case class CannotCreateException(message: String)
  extends PersistException(message)

final case class CannotReadException(message: String)
  extends PersistException(message)

final case class CannotUpdateException(message: String)
  extends PersistException(message)

final case class CannotDeleteException(message: String)
  extends PersistException(message)
