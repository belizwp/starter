package definitions.exceptions

abstract class FacadeException(message: String)
  extends Exception(message)

abstract class BadRequestException(message: String)
  extends FacadeException(message)

abstract class NotAuthenticatedException(message: String)
  extends FacadeException(message)

abstract class ForbiddenRequestException(message: String)
  extends FacadeException(message)

abstract class NotFoundException(message: String)
  extends FacadeException(message)

abstract class InternalErrorException(message: String)
  extends FacadeException(message)
