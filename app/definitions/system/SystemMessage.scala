package definitions.system

import utilities.Config

object SystemMessage {

  val SERVER_VERSION = Config("version")

  val SERVER_NAME = Config("name")

}
