package definitions.system

object HttpHeaders {

  val AGENT = "User-Agent"

  val CACHE_CONTROL = "Cache-Control"

  val MAX_AGE = "max-age: 100"

  val TOKEN = "X-Access-Token"

  val LINE_SIGNATURE = "X-Line-Signature"

  val APPLICATION_JSON = "application/json"

  val AUTHORIZATION = "Authorization"

  val CONTENT_TYPE = "Content-Type"

  val APPLICATION_FORM = "application/x-www-form-urlencoded"

  val BEARER = "Bearer"

}
