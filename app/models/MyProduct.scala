package models

import entities.{ProductEntity, UserEntity}

final case class MyProduct(id: String
                           , name: String
                           , price: Long
                           , seller: String)

object MyProduct {

  def apply(product: ProductEntity, seller: UserEntity): MyProduct =
    MyProduct(
      id = product.id
      , name = product.name
      , price = product.price
      , seller = seller.username
    )

}
