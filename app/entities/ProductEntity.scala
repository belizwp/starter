package entities

final case class ProductEntity(id: String
                               , name: String
                               , price: Long
                               , userId: String)
