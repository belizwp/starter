package entities

final case class UserEntity(id: String
                            , username: String
                            , password: String)
